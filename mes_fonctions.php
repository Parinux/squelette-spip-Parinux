<?php

define("MES", "MES");


/*************************************************************************
* Balises  #MES_
**************************************************************************/

function balise_MES_DATE($p) {
    $datedebut = "''";
    $datefin = "''";
    if (($v = interprete_argument_balise(1,$p))!==NULL){
         $datedebut = $v;
        if (($v = interprete_argument_balise(2,$p))!==NULL) {
             $datefin = $v;
        }
    }
    $p->code = "mes_balise_date($datedebut, $datefin)";
    return $p;
}


/*************************************************************************
*Fonctions de résolution des balises
**************************************************************************/

function mes_balise_date($datedebut, $datefin) {
	$texte = "";
	if (substr($datedebut, 0, 10) == substr($datefin, 0, 10)) {
		$texte .= "Le ";
		$texte .= nom_jour($datedebut);
		$texte .= " ";
		$texte .= affdate_court($datedebut);
	} else {
	
	}
	return $texte;
}