# Parinux todo list

- [x]  virer les CDN

- [ ] autres asso, sous nouveauté, liens vers assos (dont April), petits bandeaux qui se composent avec le logo du site référencé et apparaissent grâce à un mot clé attribué : "sites accueil"

- [x] Nouveauté devient :Titre : Agenda du libre /Sous-titre :en région IDF

- [x] Plan du site à traiter

- [x] filtre pour rendre inaccessible (ou plugin ??) la rubrique meta sur toute la partie publique

- [x] Plus de contraste sur typos bandeau et pour le titre du site

- [x] Lien vers l'accueil sur logo et titre du site.

- [x] largeur max un peu plus grande pour le site

- [x] Supprimer les infos d'auteur mais ajouter Date de Mise à jour

- [x] Faire disparaitre les mots clés des pages articles

- [x] pied de page : ajouter mentions légales


- [x] Couleurs

- [/] Carousel
